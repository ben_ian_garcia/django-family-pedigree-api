from django.contrib.auth.models import User

from rest_framework import serializers

from .models import Account, Member

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']

class AccountSerializer(serializers.ModelSerializer):
    birthday = serializers.DateField(format="%b %d, %Y")
    vital_status = serializers.SerializerMethodField()
    class Meta:
        model = Account
        fields = ['id', 'user' , 'birthday', 'vital_status', 'date_created']

    def get_vital_status(self,obj):
        return obj.get_vital_status_display()

class MemberSerializer(serializers.ModelSerializer):
    relationship = serializers.SerializerMethodField()
    rel_id = serializers.SerializerMethodField()
    birthday = serializers.DateField(format="%b %d, %Y")
    vital_status = serializers.SerializerMethodField()
    vs_id = serializers.SerializerMethodField()
    class Meta:
        model = Member
        fields = ['id', 'account_id', 'relationship', 'rel_id', 'first_name', 'last_name', 'birthday', 'vital_status', 'vs_id']

    def get_relationship(self,obj):
        return obj.get_relationship_display()

    def get_rel_id(self,obj):
        return str(obj.relationship)

    def get_vital_status(self,obj):
        return obj.get_vital_status_display()

    def get_vs_id(self,obj):
        return str(obj.vital_status)