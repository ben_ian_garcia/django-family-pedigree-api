from django.apps import AppConfig


class FamilyPedigreeApiConfig(AppConfig):
    name = 'family_pedigree_API'
