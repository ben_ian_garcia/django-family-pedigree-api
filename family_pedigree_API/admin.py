from django.contrib import admin
from .models import Account, Member

class ChoiceInLine(admin.TabularInline):
    model = Member
    extra = 2

class AccountAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Account Information',{'fields': ['user', 'birthday', 'vital_status']}),
    ]
    list_display =('user', 'birthday', 'vital_status')
    inlines = [ChoiceInLine]


class MemberAdmin(admin.ModelAdmin):
    list_display =('first_name', 'last_name', 'relationship', 'birthday', 'vital_status')

admin.site.register(Account, AccountAdmin)
admin.site.register(Member, MemberAdmin)