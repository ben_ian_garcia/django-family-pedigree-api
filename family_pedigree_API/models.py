from django.db import models

VITAL_STATUS_CHOICES = (
    ('A', 'Alive'),
    ('D', 'Deceased'),
    ('U', 'Unknown'),
)

class Account(models.Model):

    user = models.OneToOneField('auth.User', null=True, blank=True, on_delete=models.CASCADE)
    birthday = models.DateField(null=True, blank=True)
    vital_status = models.CharField(max_length=30, default="Alive", choices=VITAL_STATUS_CHOICES)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        if self.user:
            user = self.user
            return '{}'.format(user.username)
        return self.pk

class Member(models.Model):
    RELATIONSHIP_CHOICES = (
        ("1", 'Father'),
        ("2", 'Mother'),
        ("3", 'Brother'),
        ("4", 'Sister'),
        ("5", 'Son'),
        ("6", 'Daughter'),
        ("7", 'Spouse'),
        ("8a", 'Paternal Grandfather'),
        ("8b", 'Maternal Grandfather'),
        ("9a", 'Paternal Grandmother'),
        ("9b", 'Maternal Grandmother'),
        ("10", 'In-law'),
        ("11", 'Grandchild'),
        ("12", 'In-law'),
        ("13a", 'Aunt'),
        ("13b", 'Uncle'),
    )
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    relationship = models.CharField(max_length=30, default="", choices=RELATIONSHIP_CHOICES)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    birthday = models.DateField(null=True, blank=True)
    vital_status = models.CharField(max_length=30, default="Alive", choices=VITAL_STATUS_CHOICES)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'