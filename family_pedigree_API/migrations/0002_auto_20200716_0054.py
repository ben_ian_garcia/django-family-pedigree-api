# Generated by Django 3.0.8 on 2020-07-15 16:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('family_pedigree_API', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='vital_status',
            field=models.CharField(choices=[('A', 'Alive'), ('D', 'Deceased'), ('U', 'Unknown')], default='Alive', max_length=30),
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('relationship', models.CharField(choices=[(1, 'Father'), (2, 'Mother'), (3, 'Brother'), (4, 'Sister'), (5, 'Son'), (6, 'Daughter'), (7, 'Spouse'), (8, 'Grandfather'), (9, 'Grandmother')], default='', max_length=30)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('birthday', models.DateField(blank=True, null=True)),
                ('vital_status', models.CharField(choices=[('A', 'Alive'), ('D', 'Deceased'), ('U', 'Unknown')], default='Alive', max_length=30)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='family_pedigree_API.Account')),
            ],
        ),
    ]
