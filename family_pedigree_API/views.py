from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND
)

from .serializers import UserSerializer, AccountSerializer, MemberSerializer
from .models import Account, Member

class UserViewSets(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    @action(methods=['POST'], detail=False)
    def create_user(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')
        email = data.get('email')
        password = data.get('password')
        first_name = data.get('first_name')
        last_name = data.get('last_name')

        user = User.objects.filter(username=username, email=email).first()

        if user:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': 'ERROR: User Already Exists.',
            }, status=HTTP_400_BAD_REQUEST)
        else:
            user = User(username=username,email=email)

            try:
                validate_password(password, user)
                user.set_password(password)
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.save()
            except ValidationError as e:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': e.messages,
                }, status=HTTP_400_BAD_REQUEST)

            if user:
                serializer = self.serializer_class(user)
                return Response(serializer.data, status=HTTP_200_OK)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: User creation failed',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def get_user(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')
        password = data.get('password')

        user = User.objects.filter(username=username).order_by("pk").first()

        if user:
            if user.check_password(password):
                serializer = self.serializer_class(user)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': "ERROR: Incorrect Password",
                }, status=HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': "ERROR: User not existing",
            }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def edit_user(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')

        user = User.objects.filter(username=username).order_by("pk").first()

        if user:
            # Fetch data from JSON
            first_name = data.get('first_name')
            last_name = data.get('last_name')
            email = data.get('email')

            # Update Member information
            userupdate = User.objects.get(username=username)
            userupdate.first_name = first_name
            userupdate.last_name = last_name
            userupdate.email = email
            userupdate.save()

            if userupdate:
                serializer = UserViewSets.serializer_class(userupdate)
                return Response(serializer.data, status=HTTP_200_OK)
        else:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': 'ERROR: User does not exist.',
            }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: User Update Failed.',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def change_pass(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username')
        old_password = data.get('old_password')
        new_password1 = data.get('new_password1')
        new_password2 = data.get('new_password2')

        user = User.objects.filter(username=username).order_by("pk").first()
        if user.check_password(old_password):
            if new_password1 == new_password2:
                print(user.set_password(new_password1))
                user.save()
                if user:
                    return Response({
                        'status': HTTP_200_OK,
                        'message': 'Password Changed Successfully!',
                    }, status=HTTP_200_OK)
                else:
                    return Response({
                        'status': HTTP_400_BAD_REQUEST,
                        'message': 'Password Change Failed!',
                    }, status=HTTP_400_BAD_REQUEST)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'Error: New passwords did not match!',
                }, status=HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'status': HTTP_400_BAD_REQUEST,
                'message': 'Wrong old password!',
            }, status=HTTP_400_BAD_REQUEST)



class AccountViewSets(viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()

    @action(methods=['POST'], detail=False)
    def create_account(self, request, *args, **kwargs):
        data = request.data
        user_id = data.get('user_id')
        birthday = data.get('birthday')

        user = User.objects.filter(id=user_id).order_by("pk").first()

        if user:
            account = Account.objects.create(user=user, birthday=birthday)
            if account:
                serializer = AccountViewSets.serializer_class(account)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: User ID not found.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Account Creation Failed.',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def get_account(self, request, *args, **kwargs):
        data = request.data
        user_id = data.get('user_id')

        user = User.objects.filter(id=user_id).order_by("pk").first()

        if user:
            account = Account.objects.get(user=user)
            if account:
                serializer = AccountViewSets.serializer_class(account)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: User has no profile linked.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Account Retrieval Failed.',
        }, status=HTTP_400_BAD_REQUEST)


class MemberViewSets(viewsets.ModelViewSet):
    serializer_class = MemberSerializer
    queryset = Member.objects.order_by("relationship")

    @action(methods=['POST'], detail=False)
    def add_member(self, request, *args, **kwargs):
        data = request.data
        account_id = data.get('account_id')
        relationship = data.get('relationship')
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        birthday = data.get('birthday')
        vital_status = data.get('vital_status')

        account = Account.objects.filter(id=account_id).order_by("pk").first()

        if account:
            member = Member.objects.create(
                account_id=account_id,
                relationship=relationship,
                first_name=first_name,
                last_name=last_name,
                birthday=birthday,
                vital_status=vital_status,
            )
            if member:
                serializer = MemberViewSets.serializer_class(member)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: Account ID not found.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Member Creation Failed.',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def get_member(self, request, *args, **kwargs):
        data = request.data
        member_id = data.get('member_id')
        account_id = data.get('account_id')

        account = Account.objects.filter(id=account_id).order_by("pk").first()

        if account:
            member = Member.objects.get(id=member_id)
            if member:
                serializer = MemberViewSets.serializer_class(member)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: Member does not exist.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Member Retrieval Failed.',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def edit_member(self, request, *args, **kwargs):
        data = request.data
        member_id = data.get('member_id')
        account_id = data.get('account_id')

        account = Account.objects.filter(id=account_id).order_by("pk").first()

        if account:
            # Fetch data from JSON
            relationship = data.get('relationship')
            first_name = data.get('first_name')
            last_name = data.get('last_name')
            birthday = data.get('birthday')
            vital_status = data.get('vital_status')

            # Update Member information
            member = Member.objects.get(id=member_id)
            member.relationship = relationship
            member.first_name = first_name
            member.last_name = last_name
            member.birthday = birthday
            member.vital_status = vital_status
            member.save()

            if member:
                serializer = MemberViewSets.serializer_class(member)
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: Member does not exist.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Member Modification Failed.',
        }, status=HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def delete_member(self, request, *args, **kwargs):
        data = request.data
        member_id = data.get('member_id')
        account_id = data.get('account_id')

        account = Account.objects.filter(id=account_id).order_by("pk").first()

        if account:
            member = Member.objects.get(id=member_id)
            member.delete()

            if member:
                return Response({
                    'status': HTTP_200_OK,
                    'message': 'Successfully Deleted Family Member.',
                }, status=HTTP_200_OK)
            else:
                return Response({
                    'status': HTTP_400_BAD_REQUEST,
                    'message': 'ERROR: Member does not exist.',
                }, status=HTTP_400_BAD_REQUEST)

        return Response({
            'status': HTTP_400_BAD_REQUEST,
            'message': 'ERROR: Member Deletion Failed.',
        }, status=HTTP_400_BAD_REQUEST)