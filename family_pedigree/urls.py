from django.contrib import admin
from django.urls import path, include

from rest_framework import routers

from family_pedigree_API.views import UserViewSets, AccountViewSets, MemberViewSets

router = routers.DefaultRouter()
router.register('users', UserViewSets, basename='users')
router.register('accounts', AccountViewSets, basename='accounts')
router.register('members', MemberViewSets, basename='members')

urlpatterns = [
    path('', include(router.urls)),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('admin/', admin.site.urls),
]
